# Python HTTP Request Template

The script is a basic template for generating custom HTTP Requests with the ability to capture the raw HTTP REQUEST and RESPONSE.  The script is built on the assumption that a single HTTP session will be used for all requests.