from requests import Request, Session
# Reference: http://stackoverflow.com/questions/20658572/python-requests-print-entire-http-request-raw/23816211#23816211

####################################################################################################
########## VARIABLES ##########
####################################################################################################
s = Session()  # HTTP Session container, used to manage cookies, session tokens and other session information

####################################################################################################
########## FUNCTIONS ##########
####################################################################################################
def getREQUESTcontent(req):
    a = '---- HTTP REQUEST ----' + '\n' 
    a += req.method + ' ' + req.url + '\n'
    for k, v in req.headers.items():
        a += k + ': ' + v + '\n'        

    a += '\n'
    a += req.body or ''
    a += '\n'
    return a
    
def getRESPONSEcontent(resp):
    a = '---- HTTP RESPONSE ----' + '\n'
    a += str(resp.status_code) + '\n'
    for k, v in resp.headers.items():
        a += k + ': ' + v + '\n'
        
    a += resp.text or ''    
    return a

def prepareHTTPRequest(session,method,url,headers,data,params):
    req = Request(method,url,headers=headers,data=postData,params=getParams)
    prepared = session.prepare_request(req)
    return prepared

def sendHTTPRequest(prepardHTTPrequest):
    resp = s.send(prepared)
    return resp

####################################################################################################
########## HTTP Requests ##########
####################################################################################################

# Get Session Token
# Example makes a request to get unauthenticated token
# Logon token grabbing can be added here
url = 'http://httpbin.org/cookies/set/sessioncookie/123456789'
s.get(url)

# Prepare request
url = 'http://httpbin.org/html'
method = 'GET'
useragent = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)'
headers = {'User-Agent':useragent}
postData = {'a':'b'}    # HTTP POST Parameters
getParams = {'a':'b'}   # HTTP GET Parameters

prepared = prepareHTTPRequest(s,method,url,headers,postData,getParams)

# Print Request
print(getREQUESTcontent(prepared))

# Send Request and get Response
response = sendHTTPRequest(prepared)

# Print Response
print(getRESPONSEcontent(response))


